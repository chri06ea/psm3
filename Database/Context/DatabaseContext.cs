﻿using System;//////
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

using Domain.Interfaces;
using Domain.Models;
using System.Linq;

namespace Database.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Assignment> Assignments { get; set; }

        public DbSet<AssignmentReply> AssignmentReplies { get; set; }

        public DbSet<AssignmentReplyVote> AssignmentReplyVotes { get; set; }

        public DbSet<ForumDirectory> ForumCategories { get; set; }

        public DbSet<ForumPost> ForumPosts { get; set; }

        public DbSet<ForumThread> ForumThreads { get; set; }
    }
}
