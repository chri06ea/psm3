﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Domain.Interfaces;
using Domain.Models;
using Domain.Extensions;
using Database.Context;
using Microsoft.EntityFrameworkCore;

namespace Database
{
    public class EFDatabase : IDatabase
    {
        private readonly DatabaseContext _context;

        public EFDatabase()
        {

#if DEBUG
            var isDevelopmentMode = true;
#else
            var isDevelopmentMode = false;
#endif

            // For release mode database is "PSM3DB", for development(Debug) its "PSM3DB_DEV"
            var connectionString = $"Server=(localdb)\\mssqllocaldb;Database=PSM3DB{(isDevelopmentMode ? "_DEV" : "")};Trusted_Connection=True;MultipleActiveResultSets=true";

            //TODO: hey
            var options = new DbContextOptionsBuilder().UseSqlServer(connectionString);

            _context = new DatabaseContext(options.Options);

            _context.Database.EnsureCreated();

            _context.Database.Migrate();

            // If debug mode is turned on, add random data into the database
            if (isDevelopmentMode)
            {
                this.Seed();
            }
        }

        public void Add(BaseModel entity)
        {
            entity.Created = DateTime.Now;

            _context.Add(entity as object);

            _context.SaveChanges();
        }

        public void Remove(BaseModel entity)
        {
            _context.Remove(entity as object);
        
            _context.SaveChanges();
        }

        public void Update(BaseModel entity)
        {
            entity.LastChanged = DateTime.Now;

            _context.Entry(entity as object).CurrentValues.SetValues(entity as object);

            _context.Entry(entity as object).State = EntityState.Modified;

            _context.SaveChanges();
        }

        public bool OptimisticOfflineUpdate(BaseModel entity)
        {
            //TODO: 

            _context.SaveChanges();
        
            return false;
        }

        public List<User> Users => _context.Users.ToList();
        public List<Assignment> Assignments => _context.Assignments.ToList();
        public List<AssignmentReply> AssignmentReplies => _context.AssignmentReplies.ToList();
        public List<AssignmentReplyVote> AssignmentReplyVotes => _context.AssignmentReplyVotes.ToList();
        public List<ForumDirectory> ForumDirectories => _context.ForumCategories.ToList();
        public List<ForumPost> ForumPosts => _context.ForumPosts.ToList();
        public List<ForumThread> ForumThreads => _context.ForumThreads.ToList();
    }
}
