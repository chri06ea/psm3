﻿using Domain;
using Domain.Extensions;
using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServerLibrary
{
    public class IdentityServerUserManager : IUserManager
    {
        private static string ConnectionString = "Server=(localdb)\\mssqllocaldb;Database=PSM3_IS;Trusted_Connection=True;MultipleActiveResultSets=true;";

        private readonly IDatabase _database;

        private readonly UserManager<IdentityUser> _userManager;

        private readonly SignInManager<IdentityUser> _signinManager;

        private readonly IHttpContextAccessor _httpContextAccessor;

        static public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<IdentityDataContext>(options =>
            {
                options.UseSqlServer(ConnectionString);
            });

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<IdentityDataContext>();
        }

        public IdentityServerUserManager(
            IDatabase database,
            IHttpContextAccessor httpContextAccessor,
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager)
        {
            _database = database;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            _signinManager = signInManager;
        }

        public User GetLoggedInUser()
        {
            var task = Task.Run(() => _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User));

            task.Wait();

            var identityUser = task.Result;

            if (identityUser == null)
                return null;

            return _database.Users.Find(x => x.Username == identityUser.UserName);
        }

        public bool IsLoggedIn()
        {
            return _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;
        }

        public void Login(string username, string password)
        {
            var task1 = Task.Run(() => _userManager.FindByNameAsync(username));

            task1.Wait();

            if(task1.Result == null)
            {
                this.ThrowValidationException("Login failed");
            }

            var task = Task.Run(()=>_signinManager.PasswordSignInAsync(task1.Result, password, false, false));

            task.Wait();

            var result = task.Result;

            if(!result.Succeeded)
            {
                this.ThrowValidationException("Login failed");
            }
        }

        public void Logout()
        {
            var task = Task.Run( ()=>_signinManager.SignOutAsync());

            task.Wait();
        }

        public void Register(string username, string password, string email)
        {
            var newUser = new IdentityUser
            {
                UserName = username,
                Email = email
            };

            var task = Task.Run(() => _userManager.CreateAsync(newUser, password));

            task.Wait();

            var result = task.Result;

            if(!result.Succeeded)
            {
                var validationResult = new ValidationException();

                foreach(var error in result.Errors.Select(x=> x.Description))
                {
                    validationResult.AddError(error);
                }

                throw validationResult;
            }

            _database.Add(new User { Username = username, Email = email });
        }
    }
}
