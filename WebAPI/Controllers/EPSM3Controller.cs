﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Dynamic;

using Domain.Models;
using Domain.Interfaces;
using Domain.DTO;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EPSM3Controller : ControllerBase
    {
        private readonly IDatabase _database;

        public EPSM3Controller(IDatabase database)
        {
            _database = database;
        }

        [HttpGet, Route("get_latest_assignments")]
        public List<AssignmentEntry> GetLatestAssignments()
        {
            var res = new List<AssignmentEntry>();

            foreach(var assignment in _database.Assignments.OrderBy(x => x.Created).Take(10).ToList())
            {
                res.Add(new AssignmentEntry { Title = assignment.Title, 
                    Author = _database.Users.Find(x=>x.Id == assignment.UserId).Username, Description = assignment.Description });
            }

            return res;
        }

        [HttpGet, Route("get_latest_forumthreads")]
        public List<ForumThreadEntry> GetLatestForumThreads()
        {
            var res = new List<ForumThreadEntry>();

            foreach (var thread in _database.ForumThreads.OrderBy(x => x.Created).Take(10).ToList())
            {
                res.Add(new ForumThreadEntry { Title = thread.Title, Author = _database.Users.Find(x=>x.Id == thread.UserId).Username});
            }

            return res;
        }

        [HttpGet, Route("get_leaderboard")]
        public List<LeaderboardEntry> GetLeaderboard()
        {
            var res = new List<LeaderboardEntry>();

            foreach (var user in _database.Users)
            {
                var score = 0;

                foreach (var assignmentReplyVote in _database.AssignmentReplyVotes)
                {
                    if (assignmentReplyVote.UserId == user.Id)
                    {
                        score += assignmentReplyVote.Vote == VoteType.Upvote ? 1 : -1;
                    }
                }

                res.Add(new LeaderboardEntry { Username = user.Username, Score = score });
            }

            res.Sort((x, y) => x.Score > y.Score ? -1 : 1);

            return res;
        }
    }
}
