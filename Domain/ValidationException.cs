﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class ValidationException : Exception
    {
        public List<(string FieldName, string ErrorMessage)> Errors { get; set; } = new List<(string FieldName, string ErrorMessage)>();

        public void AddError(string errorMessage)
        {
            Errors.Add((string.Empty, errorMessage));
        }

        public void AddError(string fieldName, string errorMessage)
        {
            Errors.Add((fieldName, errorMessage));
        }
    }
}
