﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DTO
{
    public class ForumThreadEntry
    {
        public string Title { get; set; }

        public string Author { get; set; }
    }
}
