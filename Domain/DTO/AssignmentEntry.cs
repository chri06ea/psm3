﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DTO
{
    public class AssignmentEntry
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Author { get; set; }
    }
}
