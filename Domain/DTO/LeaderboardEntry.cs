﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DTO
{
    public class LeaderboardEntry
    {
        public string Username { get; set; }

        public int Score { get; set; }
    }
}
