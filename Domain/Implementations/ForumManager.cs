﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Domain.Models;
using Domain.Interfaces;
using Domain.Extensions;

namespace Domain.Implementations
{
    public class ForumManager : IForumManager
    {
        private readonly IDatabase _database;

        private readonly IUserManager _userManager;

        public ForumManager(IDatabase database, IUserManager userManager)
        {
            _database = database;

            _userManager = userManager;
        }

        public void CreateDirectory(ForumDirectory forumDirectory)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("Du skal være logget ind for at bruge denne funktionalitet");

            if (user.Type != UserType.Administrator)
                this.ThrowValidationException("Kun administratore kan oprette forum mapper");

            forumDirectory.Validate();

            _database.Add(forumDirectory);
        }

        public void CreatePost(ForumPost forumPost)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("Du skal være logget ind for at bruge denne funktionalitet");

            var thread = _database.ForumThreads.Find(x => x.Id == forumPost.ThreadId);

            if(thread == null)
                this.ThrowValidationException("Posten skal have en gyldig tråd");

            forumPost.UserId = user.Id;

            forumPost.Validate();

            _database.Add(forumPost);
        }

        public void CreateThread(ForumThread forumThread)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("Du skal være logget ind for at bruge denne funktionalitet");

            forumThread.UserId = user.Id;

            forumThread.Validate();

            _database.Add(forumThread);
        }

        public void DeleteDirectory(ForumDirectory forumDirectory)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("Du skal være logget ind for at bruge denne funktionalitet");

            if (user.Type != UserType.Administrator)
                this.ThrowValidationException("Kun admins kan slette forum mapper");

            var directoryToDelete = _database.ForumDirectories.Find(x => x.Id == forumDirectory.Id);

            if (directoryToDelete == null)
                this.ThrowValidationException("Forum mappen du forsøger at slette eksistere ikke");

            foreach(var thread in _database.ForumThreads.FindAll(x => x.DirectoryId == forumDirectory.Id))
            {
                foreach (var post in _database.ForumPosts.FindAll(x => x.ThreadId == thread.Id))
                {
                    _database.Remove(post);
                }

                _database.Remove(thread);
            }

            _database.Remove(directoryToDelete);
        }

        public void DeletePost(ForumPost forumPost)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("Du skal være logget ind for at bruge denne funktionalitet");

            var dbEntry = _database.ForumPosts.Find(x => x.Id == forumPost.Id);

            if(dbEntry == null)
                this.ThrowValidationException("Forum posten du forsøger at slette eksistere ikke");

            if (user.Id != dbEntry.UserId)
                this.ThrowValidationException("Du kan ikke slette poster du ikke ejer");

            _database.Remove(dbEntry);
        }

        public void DeleteThread(ForumThread forumThread)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("Du skal være logget ind for at bruge denne funktionalitet");

            var dbEntry = _database.ForumThreads.Find(x => x.Id == forumThread.Id);

            if(dbEntry == null)
                this.ThrowValidationException("Forum tråden du forsøger at slette eksistere ikke");
            
            if (user.Id != dbEntry.UserId)
                this.ThrowValidationException("Du kan ikke slette tråde du ikke ejer");

            foreach(var post in _database.ForumPosts.FindAll(x=>x.ThreadId == dbEntry.Id))
            {
                _database.Remove(post);
            }

            _database.Remove(dbEntry);
        }

        public void EditDirectory(ForumDirectory forumDirectory)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("Du skal være logget ind for at bruge denne funktionalitet");

            var forumDirectoryToUpdate = _database.ForumDirectories.Find(x => x.Id == forumDirectory.Id);

            if (forumDirectoryToUpdate == null)
                this.ThrowValidationException("Forum mappen du prøver at redigere eksistere ikke");

            if (user.Type != UserType.Administrator)
                this.ThrowValidationException("Kun admins kan lave forum mapper");

            forumDirectoryToUpdate.Name = forumDirectory.Name;

            forumDirectoryToUpdate.Description = forumDirectory.Description;

            forumDirectoryToUpdate.Validate();

            //TODO: Add business rules
            _database.Update(forumDirectoryToUpdate);
        }

        public void EditPost(ForumPost forumPost)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("Du skal være logget ind for at bruge denne funktionalitet");

            var postToEdit = _database.ForumPosts.Find(x => x.Id == forumPost.Id);

            if(postToEdit == null)
                this.ThrowValidationException("Posten du forsøger at ændre eksistere ikke");

            if (postToEdit.UserId != user.Id)
                this.ThrowValidationException("Du ejer ikke posten du forsøger at redigere");

            postToEdit.Text = forumPost.Text;

            postToEdit.Validate();

            //TODO: Add business rules
            _database.Update(postToEdit);
        }

        public void EditThread(ForumThread forumThread)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("Du skal være logget ind for at bruge denne funktionalitet");

            var threadToEdit = _database.ForumThreads.Find(x => x.Id == forumThread.Id);

            if (threadToEdit == null)
                this.ThrowValidationException("Tråden forsøgt ændret eksistere ikke");

            if (threadToEdit.UserId != user.Id)
                this.ThrowValidationException("Du kan kun ændre tråde du ejer");

            threadToEdit.Title = forumThread.Title;

            threadToEdit.Validate();

            _database.Update(threadToEdit);
        }
    }
}
