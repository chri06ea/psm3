﻿using Domain.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

using Domain.Extensions;

namespace Domain.Implementations
{
    public class AssignmentManager : IAssignmentManager
    {
        private readonly IUserManager _userManager;

        private readonly IDatabase _database;

        public AssignmentManager(IUserManager userManager, IDatabase database)
        {
            _userManager = userManager;

            _database = database;
        }

        public void CreateAssignment(Assignment assignment)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("You must be logged in to create assignments");

            assignment.UserId = user.Id;

            assignment.Validate();

            _database.Add(assignment);
        }

        public void CreateAssignmentReply(AssignmentReply assignmentReply)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("You must be logged in to create assignments");

            var assignment = _database.Assignments.Find(x => x.Id == assignmentReply.AssignmentId);

            if (assignment == null)
                this.ThrowValidationException("Besvarelsen skal have ne korresponderende opgave");

            assignmentReply.UserId = user.Id;

            assignmentReply.Validate();

            _database.Add(assignmentReply);
        }

        public void CreateAssignmentReplyVote(AssignmentReplyVote assignmentReplyVote)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("You must be logged in to create assignment replies");

            assignmentReplyVote.UserId = user.Id;
            
            if (_database.AssignmentReplyVotes.Find(x => x.UserId == user.Id &&
                 x.AssignmentReplyId == assignmentReplyVote.AssignmentReplyId) != null)
                this.ThrowValidationException("You can only vote once per assignment reply");

            assignmentReplyVote.Validate();

            _database.Add(assignmentReplyVote);
        }


        public void DeleteAssignment(Assignment assignment)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("You must be logged in to delete assignments");

            var assignmentToDelete = _database.Assignments.Find(x => x.Id == assignment.Id);

            if (assignmentToDelete == null)
                this.ThrowValidationException("Assignment for deletion does not exist");

            if (assignmentToDelete.UserId != user.Id)
                this.ThrowValidationException("You cannot delete assignments you do not own");

            foreach(var assignmentReply in _database.AssignmentReplies)
            {
                if(assignmentReply.AssignmentId == assignmentToDelete.Id)
                {
                    foreach (var assignmentReplyVote in _database.AssignmentReplyVotes)
                        if (assignmentReplyVote.AssignmentReplyId == assignmentReply.Id)
                            _database.Remove(assignmentReplyVote);

                    _database.Remove(assignmentReply);
                }
            }

            _database.Remove(assignmentToDelete);
        }

        public void DeleteAssignmentReply(AssignmentReply assignmentReply)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("You must be logged in to delete assignment replies");

            var assignmentReplyToDelete = _database.AssignmentReplies.Find(x => x.Id == assignmentReply.Id);

            if (assignmentReplyToDelete == null)
                this.ThrowValidationException("AssignmentReply for deletion does not exist");

            if (assignmentReplyToDelete.UserId != user.Id)
                this.ThrowValidationException("You cannot delete assignment replies you do not own");

            foreach (var assignmentReplyVote in _database.AssignmentReplyVotes)
                if (assignmentReplyVote.AssignmentReplyId == assignmentReplyToDelete.Id)
                    _database.Remove(assignmentReplyVote);

            _database.Remove(assignmentReplyToDelete);
        }

        public void EditAssignment(Assignment assignment)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("You must be logged in to create assignments");

            var assignmentToEdit = _database.Assignments.Find(x => x.Id == assignment.Id);

            if(assignmentToEdit == null)
                this.ThrowValidationException("AssignmentReply for edit does not exist");

            if (assignment.UserId != user.Id)
                this.ThrowValidationException("You cannot delete assignment replies you do not own");

            assignmentToEdit.Title = assignment.Title;

            assignmentToEdit.Description = assignment.Description;

            assignmentToEdit.Validate();

            _database.Update(assignmentToEdit);
        }

        public void EditAssignmentReply(AssignmentReply assignmentReply)
        {
            var user = _userManager.GetLoggedInUser();

            if (user == null)
                this.ThrowValidationException("You must be logged in to create assignments");

            var assignmentReplyToEdit = _database.AssignmentReplies.Find(x => x.Id == assignmentReply.Id);

            if (assignmentReplyToEdit == null)
                this.ThrowValidationException("assignmentReplyReply for edit does not exist");

            if (assignmentReplyToEdit.UserId != user.Id)
                this.ThrowValidationException("You cannot delete assignmentReply replies you do not own");

            assignmentReplyToEdit.Text = assignmentReply.Text;

            assignmentReplyToEdit.Validate();

            _database.Update(assignmentReplyToEdit);
        }
    }
}
