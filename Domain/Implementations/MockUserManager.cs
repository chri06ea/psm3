﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

using Domain.Interfaces;
using Domain.Models;

namespace Domain.Implementations
{



    public class MockUserManager : IUserManager
    {
        private User _currentUser;

        private readonly IDatabase _database;

        public MockUserManager(IDatabase database)
        {
            _database = database;
        }

        public User GetLoggedInUser()
        {
            return _currentUser;
        }

        public bool IsLoggedIn()
        {
            return _currentUser != null;
        }

        public void Login(string username, string password)
        {
            var vr = new ValidationException();
            
            //var user = new User { Username = username, PasswordHash = password, Email = email };

            if(IsLoggedIn())
            {
                vr.AddError("Already logged in");

                throw vr;
            }
            
            var user = _database.Users.ToList().Find(x => x.Username == username);

            if(user == null)
            {
                vr.AddError("Username", "User does not exist");

                throw vr;
            }

            //identityServer.PasswordSigninAsync(.........)

            _currentUser = user;
        }

        public void Logout()
        {
            _currentUser = null;
        }

        public void Register(string username, string password, string email)
        {
            var vr = new ValidationException();

            var user = new User { Username = username, PasswordHash = password, Email = email };

            _database.Add(user);

            _currentUser = user;
        }
    }
}
