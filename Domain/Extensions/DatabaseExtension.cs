﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Domain.Interfaces;
using Domain.Models;

namespace Domain.Extensions
{
    public static class DatabaseExtension
    {

        public static bool OptimisticOfflineUpdate(this IDatabase database, BaseModel entity)
        {
            // Check entity.LastChanged against db.entity.LastChanged
            // If same, update and return true
            // If not same, return false

            return false;
        }

        public static void Seed(this IDatabase database)
        {
            Random _random = new Random();

            // If database already contains data, don't seed
            if (database.Users.Count() > 0)
                return;

            database.Add(new User { Username = "chr", Email = "Email@email.com", PasswordHash = "Pass", Type = UserType.Administrator });
            database.Add(new User { Username = "jan", Email = "Email@email.com", PasswordHash = "Pass" });
            database.Add(new User { Username = "torben", Email = "Email@email.com", PasswordHash = "Pass" });
            database.Add(new User { Username = "matias", Email = "Email@email.com", PasswordHash = "Pass" });
            database.Add(new User { Username = "jakob", Email = "Email@email.com", PasswordHash = "Pass" });
            database.Add(new User { Username = "per", Email = "Email@email.com", PasswordHash = "Pass" });
            database.Add(new User { Username = "emil", Email = "Email@email.com", PasswordHash = "Pass" });
            database.Add(new User { Username = "mogens", Email = "Email@email.com", PasswordHash = "Pass" });
            database.Add(new User { Username = "dan", Email = "Email@email.com", PasswordHash = "Pass" });
            database.Add(new User { Username = "markus", Email = "Email@email.com", PasswordHash = "Pass" });

            database.Add(new Assignment { Title = "Find areal på denne figur", Description = "Figuren er en rektangel - den ene side er 5 cm, den anden 10 cm. Hvad er arealet?", UserId = 1 });
            database.Add(new Assignment { Title = "Grader af ret vinkel", Description = "Hvor mange grader er en ret vinkel?", UserId = 2 });

            database.Add(new AssignmentReply{AssignmentId = 1, UserId = 5, Text = "Hvis man ganger siderne med hinanden får man arealet: 5*10 = 50cm2"});

            database.Add(new AssignmentReplyVote{UserId = 6, Vote = VoteType.Upvote, AssignmentReplyId = 1});


            database.Add(new ForumDirectory { Name = "General", Description= "Alt vedrørende denne website." });
            database.Add(new ForumDirectory { Name = "Off-topic", Description= "Her kan du snakke om vind og vejr" });
            database.Add(new ForumDirectory { Name = "Algebra", Description= "For indhold vedrørende algebra" });
            database.Add(new ForumDirectory { Name = "Geometri", Description = "For indhold vedrørende geometri" });

            database.Add(new ForumThread { Title = "Ny startet forum", DirectoryId = 1, UserId = 1 });
            database.Add(new ForumThread { Title = "Jeg har problemer med login", DirectoryId = 1, UserId = 2 });

            database.Add(new ForumThread { Title = "Billeder af juletræer", DirectoryId = 2, UserId = 4 });
            database.Add(new ForumThread { Title = "Hurtig bil", DirectoryId = 2, UserId = 5 });

            database.Add(new ForumThread { Title = "Hvordan finder jeg X når y er 2?", DirectoryId = 3, UserId = 8 });
            database.Add(new ForumThread { Title = "X,Y,Z hvad kommer derefter?", DirectoryId = 3, UserId = 2 });

            database.Add(new ForumThread { Title = "Hvordan finder jeg arealet af en cirkel?", DirectoryId = 4, UserId = 1 });
            database.Add(new ForumThread { Title = "Er dette en spids vinkel?", DirectoryId = 4, UserId = 2 });

            foreach(var forumThread in database.ForumThreads)
            {
                database.Add(new ForumPost { Text = "Hej. Her er min tråd", ThreadId = forumThread.Id, UserId = forumThread.UserId });
                database.Add(new ForumPost { Text = "Svar på tråd", ThreadId = forumThread.Id, UserId = forumThread.UserId });
                database.Add(new ForumPost { Text = "Hej", ThreadId = forumThread.Id, UserId = forumThread.UserId });
                database.Add(new ForumPost { Text = "Løsningsforslag", ThreadId = forumThread.Id, UserId = forumThread.UserId });
            }

            //TODO: Add forum seeding
        }
    }
}
