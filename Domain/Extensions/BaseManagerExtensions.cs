﻿using System;
using System.Collections.Generic;
using System.Text;

using Domain.Interfaces;

namespace Domain.Extensions
{
    public static class BaseManagerExtensions
    {
        public static void ThrowValidationException(this IBaseManager baseManager, string error)
        {
            var validationException = new ValidationException();

            validationException.AddError(error);

            throw validationException;
        }
    }
}
