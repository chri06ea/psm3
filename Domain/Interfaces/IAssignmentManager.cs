﻿using System;
using System.Collections.Generic;
using System.Text;

using Domain.Models;

namespace Domain.Interfaces
{
    public interface IAssignmentManager : IBaseManager
    {
        // Usecase: 
        //      Opret matematik opgave
        // Preconditions:
        //      Bruger er logget ind
        // Postconditions:
        //      Opgave er blevet oprettet
        void CreateAssignment(Assignment assignment);

        // CrossRef:
        //      Opret matematik opgave
        // Preconditions:
        //      Bruger er logget ind
        //      Bruger ejer opgaven der forsøges rettet
        // Postconditions:
        //      Opgaven er blevet opdateret
        void EditAssignment(Assignment assignment);

        // CrossRef:
        //      Opret matematik opgave
        // Preconditions:
        //      Bruger er logget ind
        //      Bruger ejer opgaven der forsøges rettet
        // Postconditions:
        //      Opgaven er blevet slettet
        //      Opgave besvarelser til opgaven er blevet slettet
        //      Vurderinger til opgaven er blevet slettet
        void DeleteAssignment(Assignment assignment);

        // Usecase:
        //      Opret svar til matematik opgave
        // Preconditions:
        //      Bruger er logget ind
        //      Besvarelse forsøgt oprettet skal have en korresponderende opgave
        // Postconditions:
        //      Svaret er blevet oprettet
        void CreateAssignmentReply(AssignmentReply assignmentReply);

        // CrossRef:
        //      Opret svar til matematik opgave
        // Preconditions:
        //      Bruger er logget ind
        //      Bruger ejer svar der forsøges rettet
        // Postconditions:
        //      Svaret er blevet opdateret
        void EditAssignmentReply(AssignmentReply assignmentReply);

        // CrossRef:
        //      Opret svar til matematik opgave
        // Preconditions:
        //      Bruger er logget ind
        //      Bruger ejer svar der forsøges slettet
        // Postconditions:
        //      Svaret er blevet slettet
        //      Vurderinger af opgave besvarelse er blevet slettet
        void DeleteAssignmentReply(AssignmentReply assignmentReply);

        // Usecase:
        //      Giv vurdering af matematik opgave besvarelse
        // Precondition:
        //      Bruger er logget ind
        //      Bruger har ikke allerede givet sin vurdering på denne opgave
        //      Vurderingen skal have en korresponderende opgave besvarelse
        // Postcondition:
        //      Vurdering er blevet oprettet
        void CreateAssignmentReplyVote(AssignmentReplyVote assignmentReplyVote);
    }
}
