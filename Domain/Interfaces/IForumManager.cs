﻿using System;
using System.Collections.Generic;
using System.Text;

using Domain.Models;

namespace Domain.Interfaces
{
    public interface IForumManager : IBaseManager
    {
        // Usecase:
        //      Opret forum mappe
        // Precondition:
        //      Bruger er logget ind
        //      Bruger er admin
        // Postcondition:
        //      Forum mappe er blevet oprettet
        void CreateDirectory(ForumDirectory forumDirectory);

        void EditDirectory(ForumDirectory forumDirectory);
        
        void DeleteDirectory(ForumDirectory forumDirectory);
     
        // Usecase:
        //      Opret forum tråd
        // Precondition:
        //      Bruger er logget ind
        //      Forum tråd skal have en korresponderende forum mappe
        // Postcondition
        //      Forum tråd er blevet oprettet
        void CreateThread(ForumThread forumThread);
        
        void EditThread(ForumThread forumThread);
        
        void DeleteThread(ForumThread forumThread);

        // Usecase:
        //      Opret indlæg i forum tråd
        // Precondition:
        //      Bruger er logget ind
        //      Indlæget har en korresponderende tråd
        // Postcondition:
        //      Indlægget er blevet oprettet
        void CreatePost(ForumPost forumPost);
        
        void EditPost(ForumPost forumPost);

        void DeletePost(ForumPost forumPost);
    }
}
