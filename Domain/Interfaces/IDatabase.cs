﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

using Domain.Models;

namespace Domain.Interfaces
{
    /// <summary>
    /// Interface for accessing database.
    /// </summary>
    public interface IDatabase
    {
        /// <summary>
        /// Add an entity to the database.
        /// Notice: Do not call if you're not in the Domain project
        /// </summary>
        /// <param name="model"></param>
        void Add(BaseModel model);

        /// <summary>
        /// Removes an entity from the database.
        /// This call will succeed aslong as the ID is valid.
        /// Notice: Do not call if you're not in the Domain project
        /// </summary>
        /// <param name="model"></param>
        void Remove(BaseModel model);

        /// <summary>
        /// Updates an entity in the database.
        /// Notice: Do not call if you're not in the Domain project
        /// </summary>
        /// <param name="model"></param>
        void Update(BaseModel model);

        public List<User> Users { get; }
        public List<Assignment> Assignments { get; }
        public List<AssignmentReply> AssignmentReplies { get; }
        public List<AssignmentReplyVote> AssignmentReplyVotes { get; }
        public List<ForumDirectory> ForumDirectories { get; }
        public List<ForumPost> ForumPosts { get; }
        public List<ForumThread> ForumThreads { get; }
    }
}
