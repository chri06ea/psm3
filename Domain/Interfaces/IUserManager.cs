﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface IUserManager : IBaseManager
    {
        // Usecase: 
        //      Opret bruger
        // Precondition: 
        //      Bruger er ikke allerede logget ind 
        //      Username er unikt
        //      Email er unik
        // Postcondition: 
        //      Bruger er blevet oprettet
        //      Bruger er logget ind
        void Register(string username, string password, string email);

        // Usecase: 
        //      Log ind
        // Preconditions:
        //      Bruger er ikke allerede logget ind
        // Postcondition:
        //      Bruger er blevet logget ind
        void Login(string username, string password);

        // Usecase: 
        //      Log ud 
        // Precondition:
        //      Bruger er logget ind
        // Postcondition:
        //      Bruger er logget ud
        void Logout();

        // CrossRef:
        // Log ind, log ud, opret bruger
        bool IsLoggedIn();

        // CrossRef:
        // Log ind, log ud, opret bruger
        User GetLoggedInUser();
    }
}
