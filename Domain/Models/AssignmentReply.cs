﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

//TODO: Data annotations and custom error messages


namespace Domain.Models
{
    public class AssignmentReply : BaseModel
    {
        [Required]
        public int AssignmentId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public string Text { get; set; }
    }
}
