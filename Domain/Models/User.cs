﻿using System;
using System.Collections.Generic;
using System.Text;

//TODO: Data annotations and custom error messages

namespace Domain.Models
{
    public enum UserType
    {
        Standard,
        Administrator
    };

    public class User : BaseModel
    {
        public string Username { get; set; }

        public string PasswordHash  { get; set; }

        public string Email { get; set; }

        public UserType Type { get; set; }
    }
}
