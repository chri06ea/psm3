﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

//TODO: Data annotations and custom error messages


namespace Domain.Models
{
    public class BaseModel
    {
        public int Id { get; set; }

        public DateTime LastChanged { get; set; }

        public DateTime Created { get; set; }

        public void Validate()
        {
            var validationContext = new ValidationContext(this, null, null);

            var validationResults = new List<ValidationResult>();

            if (!Validator.TryValidateObject(this, validationContext, validationResults, true))
            {
                var validationException = new ValidationException();

                foreach (var validationResult in validationResults)
                {
                    foreach (var mn in validationResult.MemberNames)
                    {
                        validationException.Errors.Add((mn, validationResult.ErrorMessage));
                    }
                }

                throw validationException;
            }
        }
    }
}
