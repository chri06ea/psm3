﻿using System;
using System.Collections.Generic;
using System.Text;

//TODO: Data annotations and custom error messages


namespace Domain.Models
{
    public class ForumThread : BaseModel
    {
        public int DirectoryId { get; set; }

        public int UserId { get; set; }

        public string Title { get; set; }
    }
}
