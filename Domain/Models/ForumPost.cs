﻿using System;
using System.Collections.Generic;
using System.Text;

//TODO: Data annotations and custom error messages

namespace Domain.Models
{
    public class ForumPost : BaseModel
    {
        public int ThreadId { get; set; }

        public int UserId { get; set; }

        public string Text { get; set; }
    }
}
