﻿using System;
using System.Collections.Generic;
using System.Text;

//TODO: Data annotations and custom error messages

namespace Domain.Models
{
    public enum VoteType
    {
        Upvote,
        Downvote
    }

    public class AssignmentReplyVote : BaseModel
    {
        public int AssignmentReplyId { get; set; }

        public int UserId { get; set; }

        public VoteType Vote { get; set; }
    }
}
