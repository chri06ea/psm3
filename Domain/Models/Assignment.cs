﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

//TODO: Data annotations and custom error messages

namespace Domain.Models
{
    public class Assignment : BaseModel
    {
        public int UserId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}
