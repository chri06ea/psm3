﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace WepApp.Extensions
{
    public static class ControllerExtension
    {
        public static bool ExecuteTask(this Controller controller, Action action)
        {
            foreach(var modelValue in controller.ModelState)
            {
                modelValue.Value.Errors.Clear();
            }

            try
            {
                action();
            }

            catch(ValidationException ex)
            {
                foreach(var vr in ex.Errors)
                {
                    controller.ModelState.AddModelError(vr.FieldName, vr.ErrorMessage);
                }

                return false;
            }

            return true;
        }
    }
}
