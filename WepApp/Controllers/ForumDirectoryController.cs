﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Domain.Models;
using Domain.Interfaces;
using WepApp.Extensions;

namespace WepApp.Controllers
{
    public class ForumDirectoryController : Controller
    {
        private readonly IDatabase _database;

        private readonly IForumManager _forumManager;

        public ForumDirectoryController(IDatabase database, IForumManager forumManager)
        {
            _database = database;

            _forumManager = forumManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_database.ForumDirectories);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(_database.ForumDirectories.Find(x=>x.Id == id));
        }
        
        [HttpGet]
        public IActionResult Delete(int id)
        {
            return View(_database.ForumDirectories.Find(x=>x.Id == id));
        }

        [HttpPost]
        public IActionResult Create(ForumDirectory forumDirectory)
        {
            if (!this.ExecuteTask(() => _forumManager.CreateDirectory(forumDirectory)))
                return View(forumDirectory);

            return RedirectToAction("Index");

        }

        [HttpPost]
        public IActionResult Edit(ForumDirectory forumDirectory)
        {
            if (!this.ExecuteTask(() => _forumManager.EditDirectory(forumDirectory)))
                return View(forumDirectory);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Delete(ForumDirectory forumDirectory)
        {
            if (!this.ExecuteTask(() => _forumManager.DeleteDirectory(forumDirectory)))
                return View(forumDirectory);

            return RedirectToAction("Index");

        }
    }
}
