﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Domain.Interfaces;
using Domain.Models;
using WepApp.Extensions;

namespace WepApp.Controllers
{
    public class AssignmentReplyController : Controller
    {
        private IDatabase _database;

        private IAssignmentManager _assignmentManager;

        public AssignmentReplyController(IDatabase database, IAssignmentManager assignmentManager)
        {
            _database = database;

            _assignmentManager = assignmentManager;
        }

        public IActionResult Index(int assignmentId)
        {
            var assignment = _database.Assignments.Find(x => x.Id == assignmentId);

            var assignmentAuthor = _database.Users.Find(x => x.Id == assignment.UserId);

            var assignmentReplyDetails = new List<(AssignmentReply Reply, User Author, int Upvotes, int Downvotes)>();

            foreach(var reply in _database.AssignmentReplies.FindAll(x => x.AssignmentId == assignmentId))
            {
                var replyAuthor = _database.Users.Find(x => x.Id == reply.UserId);

                var upvotes = _database.AssignmentReplyVotes.Where(x => x.AssignmentReplyId == reply.Id)
                    .Count(x => x.Vote == VoteType.Upvote);

                var downvotes = _database.AssignmentReplyVotes.Where(x => x.AssignmentReplyId == reply.Id)
                    .Count(x => x.Vote == VoteType.Downvote);

                assignmentReplyDetails.Add((reply, replyAuthor, upvotes, downvotes));
            }

            return View(((assignment, assignmentAuthor), assignmentReplyDetails));
        }

        [HttpGet]
        public IActionResult Create(int assignmentId)
        {
            return View(new AssignmentReply { AssignmentId = assignmentId });
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(_database.AssignmentReplies.Find(x => x.Id == id));
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            return View(_database.AssignmentReplies.Find(x => x.Id == id));
        }

        [HttpPost]
        public IActionResult Create(AssignmentReply assignmentReply)
        {
            if(!this.ExecuteTask(() => _assignmentManager.CreateAssignmentReply(assignmentReply)))
                return View(assignmentReply);

            return RedirectToAction("Index", new { assignmentId = assignmentReply.AssignmentId });
        }

        [HttpPost]
        public IActionResult Edit(AssignmentReply assignmentReply)
        {
            if (!this.ExecuteTask(() => _assignmentManager.EditAssignmentReply(assignmentReply)))
                return View(assignmentReply);

            return RedirectToAction("Index", new { assignmentId = assignmentReply.AssignmentId });
        }

        [HttpPost]
        public IActionResult Delete(AssignmentReply assignmentReply)
        {
            if (!this.ExecuteTask(() => _assignmentManager.DeleteAssignmentReply(assignmentReply)))
                return View(assignmentReply);

            return RedirectToAction("Index", new { assignmentId = assignmentReply.AssignmentId });
        }
    }
}
