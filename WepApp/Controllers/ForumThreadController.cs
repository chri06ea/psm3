﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Domain.Interfaces;
using Domain.Models;
using WepApp.Extensions;

namespace WepApp.Controllers
{
    public class ForumThreadController : Controller
    {
        private IForumManager _forumManager;

        private IDatabase _database;

        public ForumThreadController(IForumManager forumManager, IDatabase database)
        {
            _forumManager = forumManager;

            _database = database;
        }

        public IActionResult Index(int directoryId)
        {
            var directory = _database.ForumDirectories.Find(x => x.Id == directoryId);

            var threads = _database.ForumThreads.FindAll(x => x.DirectoryId == directoryId);

            var userThreads = new List<(ForumThread, User)>();

            foreach(var thread in threads)
            {
                userThreads.Add((thread, _database.Users.Find(x => x.Id == thread.UserId)));
            }


            return View((directory, userThreads));
        }

        [HttpGet]
        public IActionResult Create(int directoryId)
        {
            return View(new ForumThread { DirectoryId = directoryId });
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(_database.ForumThreads.Find(x => x.Id == id));
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            return View(_database.ForumThreads.Find(x => x.Id == id));
        }

        [HttpPost]
        public IActionResult Create(ForumThread forumThread)
        {
            if (!this.ExecuteTask(() => _forumManager.CreateThread(forumThread)))
                return View(forumThread);

            return RedirectToAction("Index", new { directoryId = forumThread.DirectoryId });
        }


        [HttpPost]
        public IActionResult Edit(ForumThread forumThread)
        {
            if (!this.ExecuteTask(() => _forumManager.EditThread(forumThread)))
                return View(forumThread);

            return RedirectToAction("Index", new { directoryId = forumThread.DirectoryId });
        }

        [HttpPost]
        public IActionResult Delete(ForumThread forumThread)
        {
            if (!this.ExecuteTask(() => _forumManager.DeleteThread(forumThread)))
                return View();

            return RedirectToAction("Index", new { directoryId = forumThread.DirectoryId });
        }
    }
}
