﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Domain.Interfaces;
using Domain.Models;
using WepApp.Extensions;

namespace WepApp.Controllers
{
    public class ForumPostController : Controller
    {
        private readonly IDatabase _database;

        private readonly IForumManager _forumManager;

        public ForumPostController(IDatabase database, IForumManager forumManager)
        {
            _database = database;

            _forumManager = forumManager;
        }

        public IActionResult Index(int threadId)
        {
            var thread = _database.ForumThreads.First(x => x.Id == threadId);

            var threadAuthor = _database.Users.First(x => x.Id == thread.UserId);
            
            var directory = _database.ForumDirectories.First(x => x.Id == thread.DirectoryId);

            var userPosts = new List<(ForumPost, User)>();

            foreach(var post in _database.ForumPosts)
            {
                if (post.ThreadId == thread.Id)
                {
                    userPosts.Add(((post, _database.Users.First(x => x.Id == post.UserId))));
                }
            }

            return View((directory, (thread, threadAuthor), userPosts));
        }

        [HttpGet]
        public IActionResult Create(int threadId)
        {
            return View(new ForumPost { ThreadId = threadId });
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(_database.ForumPosts.Find(x=>x.Id == id));
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            return View(_database.ForumPosts.Find(x => x.Id == id));
        }

        [HttpPost]
        public IActionResult Create(ForumPost forumPost)
        {
            if (!this.ExecuteTask(() => _forumManager.CreatePost(forumPost)))
            {
                return View();
            }

            return RedirectToAction("Index", new { threadId = forumPost.ThreadId });
        }

        [HttpPost]
        public IActionResult Edit(ForumPost forumPost)
        {
            if (!this.ExecuteTask(() => _forumManager.EditPost(forumPost)))
                return View(forumPost);

            return RedirectToAction("Index", new { threadId = forumPost.ThreadId });
        }

        [HttpPost]
        public IActionResult Delete(ForumPost forumPost)
        {
            if (!this.ExecuteTask(() => _forumManager.DeletePost(forumPost)))
                return View(forumPost);

            return RedirectToAction("Index", new { threadId = forumPost.ThreadId });
        }
    }
}
