﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Domain.Models;
using WepApp.Extensions;
using Domain.Interfaces;

namespace WepApp.Controllers
{
    public class AssignmentReplyVoteController : Controller
    {
        private readonly IAssignmentManager _assignmentManager;

        private readonly IDatabase _database;

        public AssignmentReplyVoteController(IAssignmentManager assignmentManager,
            IDatabase database)
        {
            _assignmentManager = assignmentManager;

            _database = database;
        }

        [HttpGet]
        public IActionResult Create(int assignmentReplyId, VoteType vote)
        {
            return View(new AssignmentReplyVote { AssignmentReplyId = assignmentReplyId, Vote = vote });
        }

        [HttpPost]
        public IActionResult Create(AssignmentReplyVote assignmentReplyVote)
        {
            if (!this.ExecuteTask(() => _assignmentManager.CreateAssignmentReplyVote(assignmentReplyVote)))
                return View(assignmentReplyVote);

            var assignmentReply = _database.AssignmentReplies.Find(x => x.Id == assignmentReplyVote.AssignmentReplyId);

            return RedirectToAction("Index", "AssignmentReply", new { assignmentId = assignmentReply.AssignmentId });
        }
    }
}
