﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;

using System.Threading.Tasks;
using Domain.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WepApp.Models;

namespace WepApp.Controllers
{
    public class HomeController : Controller
    {
        private HttpClient _httpClient;

        public HomeController()
        {
            _httpClient = new HttpClient();
        }


        public async Task<IActionResult> Index()
        {
            var latestForumThreads = new List<ForumThreadEntry>();
            var leaderboard = new List<LeaderboardEntry>();
            var latestAssignments = new List<AssignmentEntry>();

            try
            {


                var resp = await _httpClient.GetAsync("https://localhost:44376/epsm3/get_leaderboard");

                if (resp.IsSuccessStatusCode)
                {
                    var res = await resp.Content.ReadAsStringAsync();

                    leaderboard = JsonConvert.DeserializeObject<List<LeaderboardEntry>>(res);
                }
                else
                {

                    throw new Exception();
                }

                resp = await _httpClient.GetAsync("https://localhost:44376/epsm3/get_latest_assignments");


                if (resp.IsSuccessStatusCode)
                {
                    var res = await resp.Content.ReadAsStringAsync();

                    latestAssignments = JsonConvert.DeserializeObject<List<AssignmentEntry>>(res);
                }
                else
                {
                    throw new Exception();
                }

                resp = await _httpClient.GetAsync("https://localhost:44376/epsm3/get_latest_forumthreads");


                if (resp.IsSuccessStatusCode)
                {
                    var res = await resp.Content.ReadAsStringAsync();

                    latestForumThreads = JsonConvert.DeserializeObject<List<ForumThreadEntry>>(res);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch(Exception e)
            {
                ModelState.AddModelError(string.Empty, "Kunne ikke forbinde til API");
            }

            return View((leaderboard, latestAssignments, latestForumThreads));
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Test2()
        {
            ModelState.AddModelError("", "BLEH");

            return View("Test1");
        }

        public IActionResult Forum()
        {
            return View();
        }

        public IActionResult Assignments()
        {
            return View();
        }

        public IActionResult User()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
