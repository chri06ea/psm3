﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Domain.Interfaces;
using WepApp.Extensions;

namespace WepApp.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserManager _userManager;

        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            var loggedInUser = _userManager.GetLoggedInUser();

            if(loggedInUser == null)
            {
                return RedirectToAction("Login");
            }

            return View(_userManager.GetLoggedInUser());
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Logout()
        {
            if(!this.ExecuteTask(()=> _userManager.Logout()))
            {
                //TODO: No view
                return Index();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            if (!this.ExecuteTask(() => _userManager.Login(username, password)))
            {
                return View();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Register(string username, string password, string email)
        {
            if (!this.ExecuteTask(()=> _userManager.Register(username, password, email)))
            {
                return View();
            }

            return RedirectToAction("Index");
        }
    }
}
