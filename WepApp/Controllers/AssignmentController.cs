﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Database.Context;
using Domain.Models;
using Domain.Interfaces;
using WepApp.Extensions;

namespace WepApp.Controllers
{
    public class AssignmentController : Controller
    {
        private readonly IDatabase _database;

        private readonly IAssignmentManager _assignmentManager;

        public AssignmentController(IDatabase database, IAssignmentManager assignmentManager)
        {
            _database = database;
         
            _assignmentManager = assignmentManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var userAssignments = new List<(Assignment, User)>();

            foreach(var assignment in _database.Assignments)
            {
                userAssignments.Add((assignment, _database.Users.First(x=>x.Id == assignment.UserId)));
            }

            return View(userAssignments);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Assignment assignment)
        {
            if(!this.ExecuteTask(()=> _assignmentManager.CreateAssignment(assignment)))
                return View();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(_database.Assignments.ToList().Find(x=>x.Id == id));
        }

        [HttpPost]
        public IActionResult Edit(Assignment assignment)
        {
            if(!this.ExecuteTask(() => _assignmentManager.EditAssignment(assignment)))
            {
                return View();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            return View(_database.Assignments.Find(x=>x.Id == id));
        }

        [HttpPost]
        public IActionResult Delete(Assignment assignment)
        {
            if (!this.ExecuteTask(() => _assignmentManager.DeleteAssignment(assignment)))
                return View(assignment);

            return RedirectToAction("Index");
        }
    }
}
